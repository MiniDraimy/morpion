package org.morpion.unitTest;

import org.junit.Assert;
import org.junit.Test;
import org.morpion.Grille;

public class GrilleTest {
    //TODO
    @Test
    public void testPrintGrille(){
        Grille grille = new Grille();
        grille.printGrille();
    }

    @Test
    public void testIsSpaceFreeFalse(){
        Grille grille = new Grille();
        grille.insertLetter('x',1);
        Assert.assertFalse(grille.isSpaceFree(1));
    }

    @Test
    public void testIsSpaceFreeTrue(){
        Grille grille = new Grille();
        grille.insertLetter('x',1);
        Assert.assertTrue(grille.isSpaceFree(3));
    }

    @Test
    public void testInsertLetter(){
        Grille grille = new Grille();
        grille.insertLetter('x',1);
    }
    @Test
    public void testDrawTrue(){
        Grille grille = new Grille();
        grille.insertLetter('x',1);
        grille.insertLetter('o',2);
        grille.insertLetter('x',3);
        grille.insertLetter('o',4);
        grille.insertLetter('x',5);
        grille.insertLetter('o',6);
        grille.insertLetter('o',7);
        grille.insertLetter('x',8);
        grille.insertLetter('o',9);
        Assert.assertTrue(grille.checkForDraw());
    }

    @Test
    public void testDrawFalse(){
        Grille grille = new Grille();
        grille.insertLetter('x',1);
        grille.insertLetter('o',2);
        grille.insertLetter('x',3);
        grille.insertLetter('o',4);
        grille.insertLetter('x',5);
        grille.insertLetter('o',6);
        grille.insertLetter('o',7);
        grille.insertLetter('x',8);
        Assert.assertFalse(grille.checkForDraw());
    }

    @Test
    public void testWinHorizontal1(){
        Grille grille = new Grille();
        grille.insertLetter('x',1);
        grille.insertLetter('x',2);
        grille.insertLetter('x',3);
        Assert.assertTrue(grille.checkForWin());
    }

    @Test
    public void testWinHorizontal2(){
        Grille grille = new Grille();
        grille.insertLetter('x',4);
        grille.insertLetter('x',5);
        grille.insertLetter('x',6);
        Assert.assertTrue(grille.checkForWin());
    }

    @Test
    public void testWinHorizontal3(){
        Grille grille = new Grille();
        grille.insertLetter('x',7);
        grille.insertLetter('x',8);
        grille.insertLetter('x',9);
        Assert.assertTrue(grille.checkForWin());
    }
    @Test
    public void testWinVertical1(){
        Grille grille = new Grille();
        grille.insertLetter('x',1);
        grille.insertLetter('x',4);
        grille.insertLetter('x',7);
        Assert.assertTrue(grille.checkForWin());
    }
    @Test
    public void testWinVertical2(){
        Grille grille = new Grille();
        grille.insertLetter('x',2);
        grille.insertLetter('x',5);
        grille.insertLetter('x',8);
        Assert.assertTrue(grille.checkForWin());
    }
    @Test
    public void testWinVertical3(){
        Grille grille = new Grille();
        grille.insertLetter('x',3);
        grille.insertLetter('x',6);
        grille.insertLetter('x',9);
        Assert.assertTrue(grille.checkForWin());
    }
    @Test
    public void testWinVDiagonal1(){
        Grille grille = new Grille();
        grille.insertLetter('x',1);
        grille.insertLetter('x',5);
        grille.insertLetter('x',9);
        Assert.assertTrue(grille.checkForWin());
    }
    @Test
    public void testWinDiagonal2(){
        Grille grille = new Grille();
        grille.insertLetter('x',3);
        grille.insertLetter('x',5);
        grille.insertLetter('x',7);
        Assert.assertTrue(grille.checkForWin());
    }



}
